README :
=====================
To build this project, you can use the given Makefile just running $ make.

If you want to build specifically Release binaries or Debug binaries, you may need to declare some targets to the Makefile.
To build debug, run : $ make playerdebug refereedebug
    => binary files will be output to bin/Debug/
To build release, run : $ make playerrealease refereerelease
    => binary files will be output to bin/Release/

You could also use the Code::Blocks project, however you will have to build each target individually : PlayerDebug and RefereeDebug AND/OR PlayerRelease and RefereeRelease accordingly to your needs.

USAGE :
=====================
Building outputs two binary files : player and shi-fu-mi.
* player is the random machine.
* shi-fu-mi launches the forks of players and is able to communicate with them. You may want to use this binary to start the application. Please, do not forget to launch it from the path of shi-fu-mi file, otherwise, it won't be able to find "player" binary file and it will crash. You may also like to play with the different options available. Enter $ shifumi -h # for further information.