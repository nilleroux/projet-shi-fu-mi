#include <stdio.h>
#include <stdlib.h>

#include "../defines.h"
#include "../debug.h"
#include "referee.h"
#include "../player/player.h"

int main(int argc, char* argv[])
{
    fflush(stdout);

    int opt; // Current argument
    unsigned int nb_players = 4, nb_sets = 10, waiting_time = 5000;
    unsigned char game_mode = 's';

    if (pipe(FILE_DESCRIPTOR) != 0)
    {
        fprintf(stderr, "Pipe error.\n");
        exit(EXIT_FAILURE);
    }

    // POSSIBLE OPTIONS
    // -h for help
    // -p [number_of_players]
    // -s [number_of_sets]
    while ((opt = getopt(argc, argv, "hp:s:m:w:")) != -1) // While there are options
    {
        switch (opt)
        {
            case 'p':   if ((nb_players = atoi(optarg)) < 2)
                        {
                            fprintf(stderr, "Please, enter a valid number of player.");
                            exit(EXIT_FAILURE);
                        } break;
            case 's':   nb_sets = atoi(optarg); break;
            case 'm':   game_mode = *optarg; break;
            case 'w':   waiting_time = atoi(optarg); break;
            case 'h':   printf("Usage : %s [-p number_of_players] [-s number_of_sets] [-w time_to_wait] [-m s/t/d] [-h]\n", argv[0]);
                        printf("-w n enables you to set a time to wait (in microseconds) before starting to referee. Please, use this if you ");
                        printf("expericence trouble with game waiting for players forever. Note that this time will be multiplied with the ");
                        printf("number of players. Default value is 5000 microseconds per player.\n");
                        printf("-m means \"mode\" and allows you to select a game mode.\n");
                        printf("\ts (default) stands for SETS mode. You can set the number of sets with -s argument.\n");
                        printf("\tt stands for TOURNAMENT mode. Players will confront in pairs until there's a winner.\n");
                        printf("\td stands for SUDDEN DEATH mode. After each hit, the player with the worst score is eliminated.\n");
                        exit(EXIT_SUCCESS);
            case '?':   if (optopt == 'p' || optopt == 's' || optopt == 'm')
                            fprintf(stderr, "Option -%c requires an argument. Use -h argument for further information.\n", optopt);
                        exit(EXIT_FAILURE);
        }
    }

    Player players[nb_players];
    initPlayers(players, nb_players);

    //void (*available_referees[])(Player*, unsigned int, unsigned int) = { &referee_sets, &referee_tournament, &referee_ };
    //(*available_referees[0])(players, nb_players, nb_sets); // Let's referee

    referee(players, nb_players, game_mode, nb_sets, waiting_time);

    exit(EXIT_SUCCESS);
}

