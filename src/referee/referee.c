/** /////////////////////// referee.c //////////////////////////
 * File including the implementation of the referee behaviour. */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>

#include "referee.h"
#include "../defines.h"
#include "../debug.h"
#include "../utils.h"


/** void referee(Player* players, unsigned int nb_players, unsigned int nb_sets, char game_mode, unsigned int waiting_time)
 * Function representing the referee behaviour, that is to say :
 * 1/ The referee defines the catchable signals from players
 * 2/ Foreach set and foreach player, it tells to players to generate their hits and then catches them
 * 3/ Updates the players scores
 * 4/ Displays the scores, determinates and displays the final winner
*/
void referee(Player* players, unsigned int nb_players, char game_mode, unsigned int nb_sets, unsigned int waiting_time)
{
    srand(time(NULL) ^ (getpid()<<16)); /* Different seed for each process (in order to get different random values for each player); */

    _DEBUG_TRACE("(PID %d) Referee setting up in %c mode. %d sets should be played if SETS mode.\n", getpid(), game_mode, nb_sets);

    usleep(waiting_time*nb_players);

    // This sigset defines each signal the referee is able to catch from players.
    // This means that the player must give its hit with these two signals.
    sigset_t allowed_sigs;
    sigemptyset(&allowed_sigs);
    sigaddset(&allowed_sigs, SIGUSR1);
    sigaddset(&allowed_sigs, SIGUSR2);
    sigprocmask(SIG_BLOCK, &allowed_sigs, NULL); // Blocking these signals to prevent asynchronicity side effects
                                                 // This enable us to manually handle signals.
    unsigned int i;

    // Initialize scores
    for (i = 0; i < nb_players; ++i)
        players[i].score = 0;

    switch (game_mode)
    {
        case 's': referee_sets_mode(players, nb_players, nb_sets, allowed_sigs); break;
        case 't': referee_tournament_mode(players, nb_players, allowed_sigs); break;
        case 'd': referee_sudden_death_mode(players, nb_players, allowed_sigs); break;
        default: referee_sets_mode(players, nb_players, nb_sets, allowed_sigs); break;
    }

}

/** void referee_sets_mode(Player* players, unsigned int nb_players, unsigned int nb_sets, sigset_t allowed_sigs)
 * SETS mode referee.
 */
void referee_sets_mode(Player* players, unsigned int nb_players, unsigned int nb_sets, sigset_t allowed_sigs)
{
    //PlayerHit players_hits[nb_players];
    unsigned int i, j; //players_scores[nb_players];

    for (j = 0; j < nb_sets; ++j)
    {
        for (i = 0; i < nb_players; ++i)
            retrieve_player_hit(players, i, allowed_sigs);

        printf("\n");

        update_players_scores(players, nb_players);

        print_choices(players, nb_players);
        print_scores(players, nb_players);

        sleep(2);
    }

    printf("\nWinner is Player %d !\n", get_winner(players, nb_players) + 1);
}

/** void referee_tournament_mode(Player* players, unsigned int nb_players, sigset_t allowed_sigs)
 * TOURNAMENT mode referee.
 */
void referee_tournament_mode(Player* players, unsigned int nb_players, sigset_t allowed_sigs)
{
    //PlayerHit players_hits[nb_players];
    unsigned int    i, j, //players_scores[nb_players];
                    nb_players_remaining = nb_players;
    unsigned int players_order[nb_players]; // Array shuffuled with players indexes

    for (i = 0; i < nb_players; ++i)
    {
        players[i].hit = NONE;
        players_order[i] = i; // Sorting every player ASC
    }

    shuffle(players_order, nb_players); // Shuffling the array

    #ifdef DEBUG
    for (i = 0; i < nb_players; ++i)
        _DEBUG_TRACE("(PID %d) Shuffle gave %d => %d.\n", getpid(), i, players_order[i]);
    #endif // DEBUG

    i = 0;

    while (1 < nb_players_remaining) // While there are no winner
    {
        if (nb_players_remaining-1 <= i) // Check if we have finished a pass
            i = 0;

        printf("Fight between player %d and player %d.\n", players_order[i]+1, players_order[i+1]+1);

        unsigned int looser = referee_tournament_round(players, nb_players, i, players_order, allowed_sigs); // launching round and getting looser
                                                                                                 // index in players_order array.

        print_choices(players, nb_players); // Print players choices

        printf("Player %d is out.\n\n", players_order[looser] + 1);

        players[players_order[i]].hit = players[players_order[i+1]].hit = NONE; // Setting back hit for the two players
        players[players_order[looser]].hit = OUT; // Setting "OUT" for the looser.

        for (j = looser; j < nb_players_remaining-1; ++j) // Remove looser (shift)
            players_order[j] = players_order[j+1];

        --nb_players_remaining;
        ++i;

        sleep(2);
    }


    printf("\nWinner is Player %d !\n", players_order[0]+1);
}

/** void referee_sudden_death_mode(Player* players, unsigned int nb_players, sigset_t allowed_sigs)
 * SUDDEN DEATH MODE REFEREE
 */
void referee_sudden_death_mode(Player* players, unsigned int nb_players, sigset_t allowed_sigs)
{
    unsigned int    i,
                    nb_players_remaining = nb_players,
                    min_index;

    for (i = 0; i < nb_players; i++)
        players[i].hit = NONE;

    while (1 < nb_players_remaining)
    {
        for (i = 0; i < nb_players; ++i)
            retrieve_player_hit(players, i, allowed_sigs);

        printf("\n");

        update_players_scores(players, nb_players);
        min_index = min_score(players, nb_players);

        print_choices(players, nb_players);
        print_scores(players, nb_players);

        if (min_index == -1)
            printf("Equality !\n");
        else
        {
            for (i = 0; i < nb_players; ++i)
            {
                if (players[i].score == players[min_index].score)
                {
                    players[i].hit = OUT;
                    printf("Player %d is out.\n", (i + 1));
                    --nb_players_remaining;
                }
            }
        }

        sleep(2);
    }

    printf("\nWinner is Player %d !\n", get_winner(players, nb_players) + 1);
}

/** unsigned int referee_tournament_round(Player* players, unsigned int nb_players, unsigned int player1,
 *                                   unsigned int* players_order, sigset_t allowed_sigs)
 * Plays a round in tournament mode (ie between two players).
 */
unsigned int referee_tournament_round(Player* players, unsigned int nb_players, unsigned int player1,
                                      unsigned int* players_order, sigset_t allowed_sigs)
{
    players[players_order[player1]].hit = players[players_order[player1+1]].hit = NONE;
    int winner = -1;

    while (winner == -1)
    {
        retrieve_player_hit(players, players_order[player1], allowed_sigs);
        retrieve_player_hit(players, players_order[player1+1], allowed_sigs);
        winner = hit_wins(players[players_order[player1]].hit, players[players_order[player1+1]].hit) ? 0 :
                    (hit_wins(players[players_order[player1+1]].hit, players[players_order[player1]].hit) ? 1 : -1);
        if (winner == -1)
        {
            print_choices(players, nb_players);
            printf("Equality !\n\n");
            sleep(2);
        }

    }

    return winner ? player1 : player1+1;
}

/** void retrieve_player_hit(Player* players, int player, sigset_t allowed_sigs)
 * Sends signals to player "player" to update his hit value.
 */
void retrieve_player_hit(Player* players, int player, sigset_t allowed_sigs)
{
    int signal_caught;

    if (players[player].hit == OUT) return;

    players[player].hit = NONE;

    _DEBUG_TRACE("(PID %d) Sending signal to pid %d.\n", getpid(), players[player].pid);

    kill(players[player].pid, SIGUSR1); // Please player, generate a hit and send the first bit of it to me
    _DEBUG_TRACE("(PID %d) Waiting for first bit.\n", getpid());
    sigwait(&allowed_sigs, &signal_caught); // Waiting for first bit
    increase_hit_buffer(signal_caught, &players[player].hit); // Updating hit accordingly to first bit

    _DEBUG_TRACE("(PID %d) Waiting for second bit.\n", getpid());
    kill(players[player].pid, SIGUSR2); // OK ! I have received an treated the first bit. Now, send me the second.
    sigwait(&allowed_sigs, &signal_caught); // Waiting for secong bit
    increase_hit_buffer(signal_caught, &players[player].hit); // Sending second bit

    ++(players[player].hit); // Hit is now between 0 and 2. We must increment it to get a real hit.

    _DEBUG_TRACE("(PID %d) Received response %d (%s).\n", getpid(), ((players[player].hit)-1), POSSIBLE_HITS_STRINGS[players[player].hit]);
}

/** void print_choices(PlayerHit* players_hits, unsigned int nb_players)
 * Displays the choices made by players
 */
void print_choices(const Player* players, unsigned int nb_players)
{
    unsigned int i;

    printf("%-25s", "Player");
    for (i = 0; i < nb_players; ++i)
        printf("%-20d", (i + 1));

    printf("\n%-25s", "Hit");
    for (i = 0; i < nb_players; ++i)
        printf("%-20s", POSSIBLE_HITS_STRINGS[players[i].hit]);

    printf("\n");
}

/** void print_scores(const unsigned int* players_scores, unsigned int nb_players)
 * Prints the scores thanks to the associated players scores table
*/

void print_scores(const Player* players, unsigned int nb_players)
{
    unsigned int i;

    printf("%-25s", "Score");
    for (i = 0; i < nb_players; ++i)
        printf("%-20d", players[i].score);

    printf("\n");
}

/** void update_players_scores(PlayerHit* players_hits, unsigned int* players_scores, unsigned int nb_players)
 * Updates the players scores array.
 */
void update_players_scores(Player* players, unsigned int nb_players)
{
    unsigned int i, j;

    for(i = 0; i < nb_players; ++i)
        for (j = 0; j < nb_players; ++j)
            if (i != j && hit_wins(players[i].hit, players[j].hit)) // if player i has won against player j, increase player i score.
                ++(players[i].score);
}

/** bool hit_wins(PlayerHit ph1, PlayerHit ph2)
 * returns true if ph1 wins between ph2.
 */
bool hit_wins(PlayerHit ph1, PlayerHit ph2)
{
    return (ph1 != NONE && ph1 != OUT
           && ((ph1 == ROCK && ph2 == SCISSORS)
           || (ph1 == PAPER && ph2 == ROCK)
           || (ph1 == SCISSORS && ph2 == PAPER)));
}

/** void increase_hit_buffer(int sig, PlayerHit* ph)
 * Increases the hit of a player */

void increase_hit_buffer(int sig, PlayerHit* ph)
{
    _DEBUG_TRACE("(PID %d) Signal %d received.\n", getpid(), sig);
    if (sig == SIGUSR2) // If SIGUSR2 was received
        (*ph)++;        // We must increase the hit
}

/** int get_winner(const Player* players, unsigned int nb_players)
 * Return the first player who has the highest score*/
int get_winner(const Player* players, unsigned int nb_players)
{
    unsigned int i;
    int max_index = 0;

	for(i = 1; i < nb_players; i++)
	    if(players[i].hit > players[max_index].hit)
	        max_index = i;

	return max_index;
}
