#ifndef REFEREE_H_INCLUDED
#define REFEREE_H_INCLUDED

#include "../defines.h"
#include "../player/player.h"

void referee(Player* players, unsigned int nb_players, char game_mode,
             unsigned int nb_sets, unsigned int waiting_time);
void referee_sets_mode(Player* players, unsigned int nb_players, unsigned int nb_sets, sigset_t allowed_sigs);
void referee_tournament_mode(Player* players, unsigned int nb_players, sigset_t allowed_sigs);
void referee_sudden_death_mode(Player* players, unsigned int nb_players, sigset_t allowed_sigs);
unsigned int referee_tournament_round(Player* players, unsigned int nb_players, unsigned int player1,
                                      unsigned int* players_order, sigset_t allowed_sigs);
void retrieve_player_hit(Player* players, int player, sigset_t allowed_sigs);
void print_choices(const Player* players, unsigned int nb_players);
void print_scores(const Player* players, unsigned int nb_players);
void update_players_scores(Player* players, unsigned int nb_players);
bool hit_wins(PlayerHit ph1, PlayerHit ph2);
void increase_hit_buffer(int sig, PlayerHit* ph);
int get_winner(const Player* players, unsigned int nb_players);
int get_tournament_winner(const Player* players, unsigned int nb_players, unsigned int current_round);

#endif

