/** //////////////// defines.h /////////////
 * File containing all the structs and variables useful in all the
 * Shi-Fu-Mi game.
*/

#ifndef DEFINES_H
#define DEFINES_H

/** Provides a simple but very powerful boolean type */
typedef enum { false, true } bool;

/** enum PlayerHit
 * Enumeration representing the possible hits in the Shi-Fu-Mi game.
 * Each hit is modelized by a number.
*/
typedef enum {
    NONE        =   0, /** No Hit. This should actually not be played. */
    ROCK        =   1, /** Rock */
    PAPER       =   2, /** Paper */
    SCISSORS    =   3, /** Scissors */
    OUT         =   4  /** Eliminated player */
} PlayerHit;

/** Definition of the path to player binary file that will be execute to emulate the player */
extern const char PLAYER_BINARY_PATH[];

/** Definition of a strings table, which matches with
 * the possible hits. */
extern const char* POSSIBLE_HITS_STRINGS[]; /** Displayable PlayerHit */

/** Definition of different game modes available as program arguments.
 *  Full strings are available with POSSIBLE_MODES_STRINGS. */
extern const char POSSIBLE_MODES[];

/** Definition of the full strings for available game modes.
 *  Corresponding "char" typed arguments ar available with POSSIBLE_MODES. */
extern const char* POSSIBLE_MODES_STRINGS[];

int FILE_DESCRIPTOR[2];

#endif
