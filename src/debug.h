/** ///////// debug.h //////////////////
 * File containing debugging macros for the software */

#ifndef DEBUG_H
#define DEBUG_H

#ifdef DEBUG
/* Macro allowing to print a debug message in the error stream */
#define _DEBUG_TRACE(fmt...) fprintf(stderr, fmt)
#else
/* When debug is not set, we should do nothing.
 * This trick allows to do nothing and build in any circumstances,
 * including single-instructions cases.
 * It will be automatically optimized by the compiler. */
#define _DEBUG_TRACE(fmt...) while(0)
#endif

#endif
