#include <string.h>
#include <stdlib.h>

#include "utils.h"

/** const char* basename(const char *path)
 * Returns the basename of a path.
 */
const char* basename(const char *path)
{
    char *base = strrchr(path, '/');
    return base ? base+1 : path;
}

/** void shuffle(unsigned int *arr, size_t n)
 * Shuffles an array of unsigned ints of size n.
 */
void shuffle(unsigned int *arr, size_t n)
{
    if (n > 1)
    {
        size_t i;
        for (i = 0; i < n - 1; i++)
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          unsigned int t = arr[j];
          arr[j] = arr[i];
          arr[i] = t;
        }
    }
}

/** unsigned int min_score(Players* players, int nb_players)
  * Returns the index of the smallest score of the players or -1 in case of equality.
*/
unsigned int min_score(Player* players, int nb_players)
{
    unsigned int i;
    int min_idx = -1;
    bool different = false;
    // Getting first playing player
    for (i = 0; i < nb_players; ++i)
        if (players[i].hit != OUT)
        {
            min_idx = i;
            break;
        }

    for (i = min_idx; i < nb_players; ++i)
    {
        if (players[i].hit != OUT)
        {
            if (players[i].hit != players[min_idx].hit)
                different = true;
            if (players[i].hit != OUT && players[i].score < players[min_idx].score)
                min_idx = i;
        }
    }
    return different ? min_idx : -1;
}
