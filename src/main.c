#include <stdio.h>
#include <stdlib.h>

#include "defines.h"
#include "debug.h"
#include "referee/referee.h"
#include "player/player.h"

void initPlayers(Player* players, unsigned int nb_players);

int main(int argc, char* argv[])
{
    fflush(stdout);

    int opt; // Current argument
    unsigned int nb_players = 4, nb_sets = 10;
    unsigned char game_mode = 's';

    // POSSIBLE OPTIONS
    // -h for help
    // -p [number_of_players]
    // -s [number_of_sets]
    while ((opt = getopt(argc, argv, "hp:s:m:")) != -1) // While there are options
    {
        switch (opt)
        {
            case 'p':   if ((nb_players = atoi(optarg)) < 2)
                            fprintf(stderr, "Please enter a valid number of player.");
                            exit(EXIT_FAILURE);
            case 's':   nb_sets = atoi(optarg); break;
            case 'm':   game_mode = *optarg; break;
            case 'h':   printf("Usage : %s [-p number_of_players] [-s number_of_sets] [-m s/t/d] [-h]\n", argv[0]);
                        printf("-m means \"mode\" and allows you to select a game mode.\n");
                        printf("\ts (default) stands for SETS mode. You can set the number of sets with -s argument.\n");
                        printf("\tt stands for TOURNAMENT mode. Players will confront in pairs until there's a winner.\n");
                        printf("\td stands for SUDDEN DEATH mode. After each hit, the player with the worst score is eliminated.\n");
                        exit(EXIT_SUCCESS);
            case '?':   if (optopt == 'p' || optopt == 's' || optopt == 'm')
                            fprintf(stderr, "Option -%c requires an argument. Use -h argument for further information.\n", optopt);
                        exit(EXIT_FAILURE);
        }
    }

    Player players[nb_players];
    initPlayers(players, nb_players);

    //void (*available_referees[])(Player*, unsigned int, unsigned int) = { &referee_sets, &referee_tournament, &referee_ };
    //(*available_referees[0])(players, nb_players, nb_sets); // Let's referee

    referee(players, nb_players, game_mode, nb_sets);

    exit(0);
}

/** void initPlayers(Player* players, unsigned int nb_players)
 * Initializes the players array with the given number of players (nb_players).
*/
void initPlayers(Player* players, unsigned int nb_players)
{
    unsigned int i;

    _DEBUG_TRACE("(PID %d) Launching forks with %d players.\n", getpid(), nb_players);

    for (i = 0; i < nb_players; ++i)
    {
        if ((players[i].pid = fork()) < 0) // If fork fails
        {
            _DEBUG_TRACE("(PID %d) Fork failure\n", getpid());
            abort();
        }
        else if (players[i].pid == 0) // Else, if we are a fork
            execl(PLAYER_BINARY_PATH, basename(PLAYER_BINARY_PATH), NULL);
    }
}

