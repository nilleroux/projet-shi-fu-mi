#include "defines.h"

const char PLAYER_BINARY_PATH[] = "player";

const char* POSSIBLE_HITS_STRINGS[] = { "None", "ROCK", "PAPER", "SCISSORS", "OUT" };

const char POSSIBLE_MODES[] = { 's', 't', 'd' };

const char* POSSIBLE_MODES_STRINGS[] = { "SETS", "TOURNAMENT", "SUDDEN DEATH" };
