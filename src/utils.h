#ifndef UTILS_H
#define UTILS_H

#include "player/player.h"

const char* basename(const char *path);
void shuffle(unsigned int *arr, size_t n);
unsigned int min_score(Player* players, int nb_players);

#endif // UTILS_H
