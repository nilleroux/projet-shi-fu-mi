#ifndef PLAYER_H
#define PLAYER_H

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#include "../defines.h"

/** struct Player
 * Struct representing a player in the Shi-Fu-Mi game.
 * pid : the PID which modelizes the player.
 * hit : the PlayerHit enumeration which represents the player hit.
*/
typedef struct
{
	pid_t pid;
	PlayerHit hit;
	unsigned int score;
} Player;

void initPlayers(Player* players, unsigned int nb_players);
void play();
void send_first_bit_handler(int sig);
void send_second_bit_handler(int sig);

#endif

